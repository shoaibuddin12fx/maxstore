
// Declare All the Components Here

Vue.component('home-component', require('./pages/home-page/HomePageComponent').default);
Vue.component('top-header', require('./components/header/TopHeaderComponent').default);
Vue.component('main-header', require('./components/header/MainHeaderComponent').default);
Vue.component('search-bar', require('./components/header/searchbar/SearchBarComponent').default);
Vue.component('banner', require('./components/banner/BannerComponent').default);
Vue.component('category-list', require('./components/category-list/CategoryListComponent').default);
Vue.component('item-box', require('./components/item-box/ItemBoxComponent').default);
Vue.component('item-wrapper', require('./components/item-wrapper/ItemWrapperComponent').default);
Vue.component('category-box', require('./components/category-box/CategoryBoxComponent').default);
Vue.component('main-footer', require('./components/footer/FooterComponent').default);
Vue.component('category-box', require('./components/category-box/CategoryBoxComponent').default);
Vue.component('product-page', require('./pages/product-page/ProductPageComponent').default);
Vue.component('cart-page', require('./pages/cart-page/CartPageComponent').default);
Vue.component('sign-in-page', require('./pages/signin-page/SignInPageComponent').default);
Vue.component('orders-page', require('./pages/orders-page/OrdersPageComponent').default);
Vue.component('checkout-page', require('./pages/checkout-page/CheckoutPageComponent').default);
Vue.component('thank-you-page', require('./pages/thankyou-page/ThankYouPageComponent').default);
Vue.component('account-page', require('./pages/account-page/AccountPageComponent').default);
Vue.component('vendor-register', require('./components/vendor-register/VendorRegisterComponent').default);
