import { EventBus } from "../eventbus";

export default class VendorRegisterService {

    constructor() {
    }

    checkIfVendorExists(obj) {

        return new Promise(async resolve => {
            let params = {
                orderBy: "email",
                equalTo: obj.email
            }

            const { data } = await window.axios.post('api/auth/checkVendor', { "vendor": params });
            let vendor = data;

            if (vendor.length !== 0) {
                console.log("VENDOR EXISTS");
                resolve(true);
            } else {
                resolve(false);
            }
        })
    }


    registerVendor(vendor) {

        return new Promise(async resolve => {
            let obj = {
                ven_name: vendor.ven_name,
                displayName: vendor.ven_name,
                shopname: vendor.ven_name,
                latitude: "",
                longitude: "",
                address: vendor.address,
                phone1: vendor.phoneNumber,
                phone2: "",
                email: vendor.email,
                logo: "",
                date: "",
                status: "InActive",
                created_at: Date.now(),
            }

            const { data } = await window.axios.post('api/auth/registerVendor', { "vendor": obj });

            console.log(data);

            resolve(data);
        })

    }

    async registerUser(form) {
        return new Promise(async resolve => {
            const { data } = await window.axios.post('/api/auth/registerUser', { "user": form });


            console.log(data);

            if (data) {
                let str = JSON.stringify(data);
                localStorage.setItem('current_user', str);
                EventBus.$emit('user:loggedIn', data);
            }

            resolve(data);
        })

    }

    async saveUser(obj) {
        return new Promise(async resolve => {
            let user = {
                id: obj.uid,
                firstName: obj.firstName,
                lastName: obj.lastName,
                createdAt: Date.now(),
            };
            const {data} = await window.axios.post('/api/auth/saveUser', {"user": user});
            console.log(data);
            resolve(data);
        })
    }

    async updateUser(obj) {
        return new Promise(async resolve => {

            let roles = {
                "vendor": true
            }

            obj["roles"] = roles;
            obj["createdAt"] = Date.now();
            const { data } = await window.axios.post('/api/auth/updateUser', { "user": obj });

            console.log(data);

            resolve(data);
        })
    }
}
