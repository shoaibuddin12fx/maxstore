/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import VueRouter from "vue-router";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import moment from "moment";
import VueSweetalert2 from 'vue-sweetalert2';

import 'sweetalert2/dist/sweetalert2.min.css';
require('./bootstrap');

window.Vue = require('vue');
Vue.mixin(require('./asset'));
Vue.use(VueRouter);
// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
// Make VueSweetAlert2 available throughout your project
Vue.use(VueSweetalert2);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

require("./components")

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const routes = [
    {path: '/', component: require('./pages/home-page/HomePageComponent').default},
    {path: '/product-page/:id', component: require('./pages/product-page/ProductPageComponent').default, props: true},
    {name: "ShopPage" , path: '/shop-page', component: require('./pages/shop-page/ShopPageComponent').default, props: true},
    // {path: '/shop-page/:id', component: require('./pages/shop-page/ShopPageComponent').default, props: true},
    {path: '/cart-page', component: require('./pages/cart-page/CartPageComponent').default},
    {path: '/checkout-page', component: require('./pages/checkout-page/CheckoutPageComponent').default},
    {path: '/sign-in', component: require('./pages/signin-page/SignInPageComponent.vue').default},
    {path: '/orders-page', component: require('./pages/orders-page/OrdersPageComponent').default},
    {path: '/thank-you/:order', component: require('./pages/thankyou-page/ThankYouPageComponent').default, props: true},
    {path: '/account', component: require('./pages/account-page/AccountPageComponent').default},
    {path: '/about-us', component: require('./pages/about-us-page/AboutUsPageComponent').default},
    {path: '/vendors-page', component: require('./pages/vendors-page/VendorsPageComponent').default},
    {path: '/vendor-register', component: require('./components/vendor-register/VendorRegisterComponent').default},
];

Vue.filter('formatDate', (value) => {
    if (value) {
        return moment.unix(value).format("MM-DD-YYYY");
    }
})

const router = new VueRouter({
    base: '/maxstore/public',
    routes: routes,
    mode: "history"
})

const app = new Vue({
    el: '#app',
    router
});
