<?php

namespace App\Http\Controllers;

use App\Helpers\OrderHelper;
use App\Helpers\FirebaseHelper;
use Illuminate\Support\Facades\Request;

class OrderController extends Controller
{

    public function placeOrder(Request $request)
    {
        $data = $request::all();
        $orderHelper = new OrderHelper();

        $order = $data["order"];

        return $orderHelper->placeOrder($order);
    }

    public function getUserOrders(Request $request)
    {
        $data = $request::all();
        $firebase = new FirebaseHelper();

        $uid = $data["uid"];

        return $firebase->getOrders($uid);
    }
}
