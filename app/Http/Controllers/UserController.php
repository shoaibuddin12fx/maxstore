<?php

namespace App\Http\Controllers;

use App\Helpers\UserHelper;
use App\Helpers\FirebaseHelper;
use Illuminate\Support\Facades\Request;

class UserController extends Controller
{

    public function signIn(Request $request)
    {
        $data = $request::all();
        $helper = new UserHelper();
        $cred = $data["cred"];

        return $helper->signIn($cred);
    }

    public function registerUser(Request $request)
    {
        $data = $request::all();
        $helper = new UserHelper();
        $user = $data["user"];
        $user["phoneNumber"] = "+92".$user["phoneNumber"];

        return $helper->registerUser($user);
    }

    public function saveUser(Request $request)
    {
        $data = $request::all();
        $helper = new UserHelper();
        $user = $data["user"];
        return $helper->saveUser($user);
    }


    public function updateUser(Request $request)
    {
        $data = $request::all();
        $helper = new UserHelper();
        $user = $data["user"];
        return $helper->updateUser($user);
    }


    public function getCurrentUser(Request $request)
    {
        $data = $request::all();
        $helper = new UserHelper();
        $uid = $data["uid"];
        return $helper->getCurrentUser($uid);
    }

    public function checkVendor(Request $request)
    {
        $data = $request::all();
        $helper = new FirebaseHelper();
        $obj = $data["vendor"];
        return $helper->checkVendor($obj);
    }

    public function registerVendor(Request $request)
    {
        $data = $request::all();
        $helper = new UserHelper();
        $obj = $data["vendor"];
        return $helper->registerVendor($obj);
    }


    public function getUserById(Request $request)
    {
        $data = $request::all();
        $helper = new UserHelper();
        $uid = $data["uid"];
        return $helper->getUserById($uid);
    }
}
