<?php

namespace App\Http\Controllers;

use App\Helpers\FirebaseHelper;
use Illuminate\Support\Facades\Request;

class FirebaseController extends Controller
{

    public function getItems(Request $request)
    {
        // filters
        $data = $request::all();
        $helper = new FirebaseHelper();
        $list = $helper->getItems();

        $filtered = collect($list);

        if (isset($data["feature"])) {
            $filtered = $filtered->where('feature', '==', true);
        }

        if (isset($data["onSale"])) {
            $filtered = $filtered->where('saleprice', '!=', 0);
        }

        if (isset($data["suggested"])) {
            $filtered = $filtered->where('feature', '==', true);
        }

        $filtered = $filtered->where('status', '==', "Active")->all();

        return $helper->objectTOArray($filtered);

    }

    public function getItemsBySearchString(Request $request)
    {
        $data = $request::all();
        $helper = new FirebaseHelper();
        $list = $helper->getItems();

        $filtered = collect($list);
        $searchTerm = $data["searchTerm"];

        $filtered = $filtered->where('name', 'like', '%' . $searchTerm . '%');

        $filtered = $filtered->where('status', '==', "Active")->all();

        return $helper->objectTOArray($filtered);
    }

    public function getPaginationItems(Request $request)
    {
        $data = $request::all();
        $last = null;
        if (isset($data["last"])) {
            $last = $data["last"];
        }

        $helper = new FirebaseHelper();
        $list = $helper->getPaginationItems($last);

        $filtered = collect($list);

        if (isset($data["feature"])) {
            $filtered = $filtered->where('feature', '==', true);
        }

        if (isset($data["parentCat"])) {
            $parentCat = $data["parentCat"];
            $id = $data["id"];

            if ($parentCat == "true") {
                $filtered = $filtered->where('categories', '==', $id);
            }

            if ($parentCat == "false") {
                $filtered = $filtered->where('sub_category', '==', $id);
            }
        }

        if (isset($data["vendorId"])) {
            $vendorId = $data["vendorId"];
            $filtered = $filtered->where('vendor', '==', $vendorId);
        }
        $filtered = $filtered->where('status', '==', "Active")->all();

        $filtered = $helper->objectTOArray($filtered);

        $array = $filtered;
        $filtered = [];
        $index = 0;

        foreach ($array as $item) {
            $index++;
            if ($index <= 20) {
                array_push($filtered, $item);
            }
        }

        return $filtered;
    }

    public function getItemById(Request $request)
    {
        $data = $request::all();
        $helper = new FirebaseHelper();
        $id = $data["itemId"];
        $item = $helper->getItemById($id);
        $item["documentId"] = $id;
        return $item;
    }

    public function getSlides(Request $request)
    {
        $data = $request::all();
        $helper = new FirebaseHelper();
        $list = $helper->getSlides();
        $key = "";
        $filtered = collect($list);

        if (isset($data["mainSlider"])) {
            $filtered = $filtered->where('type', '==', "website_banner_1" || "website_banner_2" || "website_banner_3" || "website_banner_4");
        }

        if (isset($data["featuredSlider"])) {
            $filtered = $filtered->where('type', '==', "website_featured_banner");
        }

        $filtered = $filtered->where('status', '==', "active");

        return $helper->objectTOArray($filtered->all());
    }

    public function getParentCategories(Request $request)
    {
        $helper = new FirebaseHelper();
        $list = $helper->getCategories();

        $categories = collect($list);
        $categories = $categories->where('parent_category_id', '==', "");
        $filtered = $categories->where('status', '==', "active")->all();

        return $filtered;
    }

    public function getChildCategories(Request $request)
    {
        $data = $request::all();
        $helper = new FirebaseHelper();
        $id = $data["id"];
        $list = $helper->getCategories();

        $categories = collect($list);
        $categories = $categories->where('parent_category_id', '==', $id);

        $filtered = $categories->where('status', '==', "active")->all();

        return $filtered;
    }

    public function getItemsByCatId(Request $request)
    {
        $data = $request::all();
        $helper = new FirebaseHelper();
        $id = $data["id"];
        $parent_cat = $data["parentCat"];
        $list = $helper->getItems();

        $filtered = collect($list);

        if ($parent_cat == "true") {
            $filtered = $filtered->where('categories', '==', $id);
        }

        if ($parent_cat == "false") {
            $filtered = $filtered->where('sub_category', '==', $id);
        }

        $filtered = $filtered->where('status', '==', "Active")->all();

        $filtered = $helper->objectTOArray($filtered);

        if (isset($data["limit"])) {
            $array = $filtered;
            $filtered = [];
            $index = 0;

            foreach ($array as $item) {
                $index++;
                if ($index <= $data["limit"]) {
                    array_push($filtered, $item);
                }
            }

        }

        return $filtered;

    }

    public function getZones(Request $request)
    {
        $data = $request::all();
        $helper = new FirebaseHelper();

        $list = $helper->getZones();

        $filtered = collect($list);

        $filtered = $filtered->where('status', '==', "Active")->all();

        return $filtered;
    }

    public function getSubZones(Request $request)
    {
        $data = $request::all();
        $helper = new FirebaseHelper();
        $list = $helper->getSubZones();
        $id = $data["id"];

        $filtered = collect($list);

        $filtered = $filtered->where('city', '==', $id);
        $filtered = $filtered->where('status', '==', "Active")->all();

        return $filtered;

    }

    public function getCategoryBannerImage(Request $request)
    {
        $data = $request::all();
        $helper = new FirebaseHelper();
        $list = $helper->getSlides();
        $id = $data["id"];

        $filtered = collect($list);

        $filtered = $filtered->where('type', '==', "website_banner" . $id);
        $filtered = $filtered->where('status', '==', "active")->all();

        return $filtered;

    }

    public function getAbout(Request $request)
    {
        $data = $request::all();
        $helper = new FirebaseHelper();

        return $helper->getAbout();
    }

    public function getVendors(Request $request)
    {
        $data = $request::all();
        $helper = new FirebaseHelper();
        $list = $helper->getVendors();

        $filtered = collect($list);

        $filtered = $filtered->where('status', '==', "active")->all();

        return $helper->objectTOArray($filtered);
    }

    public function getCategoryById(Request $request)
    {
        $data = $request::all();
        $helper = new FirebaseHelper();
        $id = $data["id"];
        $category = $helper->getCategoryById($id);
        $category["documentId"] = $id;
        return $category;
    }
}
