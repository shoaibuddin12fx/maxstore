<?php


namespace App\Helpers;

use Illuminate\Support\Facades\Session;

class OrderHelper
{

    protected $database;

    public function __construct()
    {
        $this->database = app('firebase.database');
    }

    public function placeOrder($order)
    {
        $orderRef = $this->database->getReference('/orders')->push($order);

        $orderKey = $orderRef->getKey();

        if ($orderKey) {
            return $orderKey;
        }
    }
}
