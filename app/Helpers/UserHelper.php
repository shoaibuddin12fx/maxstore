<?php


namespace App\Helpers;

class UserHelper
{
    protected $auth;

    public function __construct()
    {
        $this->auth = app('firebase.auth');
    }

    public function signIn($obj)
    {

        $email = $obj["email"];
        $password = $obj["password"];

        $signInResult = $this->auth->signInWithEmailAndPassword($email, $password);

        return $signInResult->data();
    }

    public function registerUser($obj)
    {
        $userProperties = [
            'email' => $obj["email"],
            'emailVerified' => false,
            'phoneNumber' => $obj["phoneNumber"],
            'password' => $obj["password"],
            'displayName' => $obj["displayName"],
            'photoUrl' => "",
            'disabled' => false,
        ];

        return $this->auth->createUser($userProperties);
    }

    public function saveUser($obj)
    {
        $database = app('firebase.database');
        $uid = $obj["id"];

        $user;
        $user["createdAt"] = $obj["createdAt"];
        $user["displayName"] = $obj["firstName"];
        $user["facebook"] = "";
        $user["lastName"] = $obj["lastName"];
        $user["address"] = "user address";
        $user["type"] = "customer";
        
        $userRef = $database->getReference("/users/$uid")->set($user);

        if ($userRef) {
            return $userRef;
        }
    }

    public function updateUser($obj)
    {
        $database = app('firebase.database');
        $uid = $obj["uid"];
        $user;
        $user["createdAt"] = $obj["createdAt"];
        $user["displayName"] = $obj["firstName"];
        $user["roles"] = $obj["roles"];
        $user["ven_name"] = $obj["ven_name"];
        $user["ven_key"] = $obj["ven_key"];
        $user["lastName"] = $obj["lastName"];
        $user["address"] = "user address";

        $userRef = $database->getReference("/users/$uid")->update($user);

        if ($userRef) {
            return $userRef;
        }
    }

    public function getCurrentUser($uid)
    {
        return $this->auth->getUser($uid);
    }

    
    public function registerVendor($vendor)
    {
        $database = app('firebase.database');
        $vendorRef = $database->getReference('/vendor')->push($vendor);

        $vendorKey = $vendorRef->getKey();

        if ($vendorKey) {
            return $vendorKey;
        }
    }
}
