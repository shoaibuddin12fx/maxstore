<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;

class FirebaseHelper
{
    protected $database;

    public function __construct()
    {
        $this->database = app('firebase.database');
    }

    public function objectTOArray($reference)
    {
        $items = [];

        foreach ($reference as $key => $value) {

            $item = $value;
            $item['documentId'] = $key;
            $items[] = $item;
        };

        return $items;
    }

    public function getItems()
    {
        $reference = $this->database->getReference('items')->getValue();

        if ($reference) {
            return $reference;
        } else {
            return [];
        }

    }

    public function getPaginationItems($last)
    {
        $reference = [];
        $key = '$key';
        if ($last) {
            $url = "https://max-store-app.firebaseio.com/items.json?orderBy=\"$key\"&startAt=\"$last\"";
            $reference = Http::get($url);
        } else {
            $reference = Http::get('https://max-store-app.firebaseio.com/items.json?orderBy="$key"');
        }

        if ($reference) {
            return $reference->json();
//            return $reference->json();
        } else {
            return [];
        }
    }


    public function checkVendor($obj)
    {
        $orderBy = $obj["orderBy"];
        $equalTo = $obj["equalTo"];

        $url = "https://max-store-app.firebaseio.com/vendor.json?orderBy=\"$orderBy\"&equalTo=\"$equalTo\"";
        $reference = Http::get($url);

        if ($reference) {
            return $reference->json();
        } else {
            return [];
        }
    }

    public function getItemById($id)
    {
        $reference = $this->database->getReference("items/$id")->getValue();

        if ($reference) {
            return $reference;
        } else {
            return null;
        }
    }


    public function getSlides()
    {
        $reference = $this->database->getReference('slider')->getValue();

        if ($reference) {
            return $this->objectTOArray($reference);
        } else {
            return [];
        }
    }

    public function getCategories()
    {
        $reference = $this->database->getReference('category')->getValue();

        if ($reference) {
            return $this->objectTOArray($reference);
        } else {
            return null;
        }
    }

    public function getZones()
    {
        $reference = $this->database->getReference('/city')->getValue();

        if ($reference) {
            return $this->objectTOArray($reference);
        } else {
            return null;
        }
    }

    public function getSubZones()
    {
        $reference = $this->database->getReference('/districts')->getValue();

        if ($reference) {
            return $this->objectTOArray($reference);
        } else {
            return null;
        }
    }

    public function getOrders($uid)
    {
        $reference = $this->database->getReference('/orders')
            ->orderByChild('user_id')
            ->equalTo($uid)
            ->getValue();

        if ($reference) {
            return $this->objectTOArray($reference);
        } else {
            return null;
        }
    }

    public function getAbout()
    {
        $reference = $this->database->getReference("about/-M9IziUyofL_5VQAyARS")->getValue();

        if ($reference) {
            return $reference;
        } else {
            return null;
        }

    }

    public function getVendors()
    {
        $reference = $this->database->getReference("/vendor")->getValue();

        if ($reference) {
            return $reference;
        } else {
            return null;
        }
    }

    public function getCategoryById($id)
    {
        $reference = $this->database->getReference("/category/$id")->getValue();

        if ($reference) {
            return $reference;
        } else {
            return null;
        }
    }
}
