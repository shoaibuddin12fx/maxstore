<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name', 'Laravel')); ?></title>
    <!-- Scripts -->
    <script defer src="<?php echo e(asset('js/app.js')); ?>"></script>
    <script>
        var laravel = <?php echo json_encode(['baseURL' => url('/')], 15, 512) ?>
    </script>
    <script>
        window._asset = '<?php echo e(asset('')); ?>';
    </script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;700;900&display=swap" rel="stylesheet">
    <!-- Styles -->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
</head>
<body>
    <div id="app">
        <top-header></top-header>
        <main-header></main-header>
        <search-bar></search-bar>
        <main style="background-color: #F2F2F2;">
            <div class="container p-0">
                <router-view></router-view>
            </div>
        </main>
        <main-footer></main-footer>
    </div>
</body>
</html>
<?php /**PATH /Applications/MAMP/htdocs/maxstore/resources/views/layouts/app.blade.php ENDPATH**/ ?>