<?php

use Illuminate\Support\Facades\Route;


/*
| Web Routes
*/

Route::get('/{vue_capture?}', function () {
    return view('home');
})->where('vue_capture', '[\/\w\.-]*');
