<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FirebaseController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('auth')->group(function () {
    Route::post('/signIn', [UserController::class, 'signIn']);
    Route::post('/registerUser', [UserController::class, 'registerUser']);
    Route::post('/saveUser', [UserController::class, 'saveUser']);
    Route::post('/updateUser', [UserController::class, 'updateUser']);
    Route::get('/getCurrentUser', [UserController::class, 'getCurrentUser']);
    Route::post('/registerVendor', [UserController::class, 'registerVendor']);
    Route::post('/checkVendor', [UserController::class, 'checkVendor']);
});

Route::prefix('firebase')->group(function () {
    Route::get('/getItems', [FirebaseController::class, 'getItems']);
    Route::get('/getPaginationItems', [FirebaseController::class, 'getPaginationItems']);
    Route::get('/getItemBySearchString', [FirebaseController::class, 'getItemsBySearchString']);
    Route::get('/getSlides', [FirebaseController::class, 'getSlides']);
    Route::get('/getParentCategories', [FirebaseController::class, 'getParentCategories']);
    Route::get('/getItemById', [FirebaseController::class, 'getItemById']);
    Route::get('/getChildCategories', [FirebaseController::class, 'getChildCategories']);
    Route::get('/getItemsByCatId', [FirebaseController::class, 'getItemsByCatId']);
    Route::get('/getZones', [FirebaseController::class, 'getZones']);
    Route::get('/getSubZones', [FirebaseController::class, 'getSubZones']);
    Route::get('/getCategoryBannerImage', [FirebaseController::class, 'getCategoryBannerImage']);
    Route::get('/getAbout', [FirebaseController::class, 'getAbout']);
    Route::get('/getVendors', [FirebaseController::class, 'getVendors']);
    Route::get('/getCategoryById', [FirebaseController::class, 'getCategoryById']);
});

Route::prefix('orders')->group(function () {
    Route::post('/placeOrder', [OrderController::class, 'placeOrder']);
    Route::get('/getUserOrders', [OrderController::class, 'getUserOrders']);
});
